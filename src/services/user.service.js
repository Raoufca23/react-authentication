import http from "../http-common";
import authHeader from './auth-header';

// const API_URL = 'http://localhost:8080/api/test/';
const API_URL = 'https://192.168.0.22:3001/api/';
// const API_URL = 'http://192.168.0.22:3000/api/test/';

class UserDataService {

  getPublicContent() {
    return http.get(API_URL + 'all');
  }

  getUserBoard() {
    return http.get(API_URL + 'user', { headers: authHeader() });
  }

  getModeratorBoard() {
    return http.get(API_URL + 'mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return http.get(API_URL + 'admin', { headers: authHeader() });
  }

  getAllOperations(id) {
    return http.get('/operations/'+ id)
  }

  logPersonneToOperation(data) {
    // return http.post(API_URL + 'personnes/operation', data, { headers : authHeader() })
    return http.post('/operations/' + data.id_operation, { id_personne :  data.id_personne })
  }

  manageSearches(id) {
    return http.get('/personnes/operation/' + id)
  }

  executeSearch(id_operation, id_recherche) {
    return http.get('/personnes/operation/' + id_operation + '/recherche/' + id_recherche)
  }

  getAllFormOperation(id) {
    return http.get('/personnes/operation/forms/' + id)
  }

}

export default new UserDataService();