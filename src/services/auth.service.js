import http from "../http-common";

class AuthService {

  async connect(data) {
    return http.post("/auth/signin", data)
  }

  async disconnect() {
    console.log('Logout')
    return http.get("/auth/signout")
                .then(response => {
                  if(response.data === 'Disconnected successfully') {
                    localStorage.removeItem("user")
                  }
                  return "Disconnected"
                })
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }

}

export default new AuthService();

