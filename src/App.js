import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

import AuthService from "./services/auth.service";

import Home from './components/home'
import Login from "./components/login";
import Profile from './components/profile'
import User from './components/user'
import Operation from './components/operation'

import { Provider } from 'react-redux'
import Store from './store/configureStore'

class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       currentUser : AuthService.getCurrentUser(),
       isLogged : false
    }
    console.log("const app")
  }

  componentDidMount() {
    console.log('mounted app')
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        isLogged : true
      });
    }
  }

  componentWillUnmount() {
    console.log('unmounted app')
  }
  

  logOut = (e) => {
    e.preventDefault()
    console.log('Logout')
    AuthService.disconnect().then(res => {
      if(res) {
        window.location.href = "/login";
      }
    });
  }
  
  render() {
    const {currentUser} = this.state
    console.log("render app")
    return (
      <Provider store={Store}>
        <Router>
          <div>
            <nav className="navbar navbar-expand navbar-dark bg-dark">
              <Link to={"/"} className="navbar-brand">
                Application
              </Link>
              <div className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={"/home"} className="nav-link">
                    Home
                  </Link>
                </li>
                {currentUser && (
                  <li className="nav-item">
                    <Link to={"/user"} className="nav-link">
                      User
                    </Link>
                  </li>
                )}
              </div>

              {currentUser ? (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link to={"/profile"} className="nav-link">
                      {currentUser.nom_personne + currentUser.prenom_personne}
                    </Link>
                  </li>
                  <li className="nav-item">
                    <a href="/login" className="nav-link" onClick={this.logOut}>
                      Logout
                    </a>
                  </li>
                </div>
              ) : (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link to={"/login"} className="nav-link">
                      Login
                    </Link>
                  </li>
                </div>
              )}
            </nav>
  
            <div className="container mt-3">
              <Switch>
                <Route exact path={["/", "/home"]}>
                  <Home data={this.state} />
                </Route>
                <Route exact path="/login" component={Login} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/user" component={User} />
                <Route exact path="/operation/:id" component={Operation} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    );
  } 
}

export default App;
