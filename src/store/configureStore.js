import { createStore } from 'redux'
import manageOperation from './reducers/operationReducer'

export default createStore(manageOperation)