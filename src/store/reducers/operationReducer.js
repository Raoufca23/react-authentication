const initialseSate = { 
  operations : [],
  currentOperation : undefined 
}

function manageOperations(state = initialseSate, action) {

  let nextState

  switch (action.type) {

    case 'LIST_OPERATIONS':
      nextState = {
        ...state,
        operations : action.value
      }
      return nextState || state

    case 'GET_CURRENT_OPERATION':
      nextState = {
        ...state,
        currentOperation : action.value
      }
      return nextState || state

    default:
      return state
  }

}

export default manageOperations