import React, { Component } from 'react'
// import UserService from "../services/user.service";
import { Redirect } from 'react-router-dom'

export default class home extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      content: "Public content",
    };
  }

  componentDidMount() {
    console.log("mount home")
    console.log(this.props)
    /*UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        console.log(error)
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );*/
    /* fetch('https://raw.githubusercontent.com/ag-grid/ag-grid/master/grid-packages/ag-grid-docs/src/sample-data/rowData.json')
    .then(result => result.json())
    .then(rowData => this.setState({rowData})) */
  }

  componentWillUnmount() {
    console.log("unmount home")
    this.setState = ()=>{
      return;
    };
  }
  
  onButtonClick = e => {
    const selectedNodes = this.gridApi.getSelectedNodes()
    const selectedData = selectedNodes.map( node => node.data )
    const selectedDataStringPresentation = selectedData.map( node => node.make + ' ' + node.model).join(', ')
    alert(`Selected nodes: ${selectedDataStringPresentation}`)
  }

  render() {
    const { currentUser, isLogged } = this.props.data
    console.log("render home")
    return (
      <div>
        {currentUser && !isLogged ? (
        <Redirect to="/profile" />
      ) : (
        <div className="container">
          <header className="jumbotron">
            <h3>{this.state.content}</h3>
          </header>
        </div>
      )}
      </div>
    )
  }
}
