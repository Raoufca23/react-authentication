import React, { Component } from 'react'

export class spinner extends Component {
  render() {
    return (
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    )
  }
}

export default spinner
