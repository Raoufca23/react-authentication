import React, { Component } from 'react'
import UserService from '../services/user.service'
import { connect } from 'react-redux'

class operation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedIn : false,
      // content : this.props.location.state.currentOperation,
      content : undefined,
      recherche : undefined,
      resultSearch : undefined,
    }
  }

  componentDidMount() {
    // let id_operation = this.props.location.state.currentOperation.id_operation
    console.log(this.props)
    
    /* UserService.manageSearches(id_operation)
      .then(result => {
        console.log(result.data)
        this.setState({
          recherche : result.data,
          rowData : result.data
        })
      }).catch(e => console.log(e.message)) */
    
  }

  handeSearch = (e) => {
    let id_recherche = parseInt(e.target.parentNode.id)
    let id_operation = this.state.content.id_operation
    UserService.executeSearch(id_operation, id_recherche)
      .then(res => {
        console.log(res.data)
        this.setState({
          resultSearch : res.data
        })
      })
      .catch(e => console.log(e.message))
  }         
  
  /* <table className="table table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nom de la recherche</th>
          <th scope="col">Affichage</th>
          <th scope="col">Date de création</th>
        </tr>
      </thead>
      <tbody>
        { 
          recherche.map((item, index) => (
              <tr key={item.id_recherche} id={item.id_recherche} onClick={this.handeSearch}>
                <th scope="row">{index + 1 }</th>
                <td>{item.nom_recherche}</td>
                <td>{item.type_recherche}</td>
                <td>{item.date_recherche}</td>
              </tr>
            )
          )
        }
      </tbody>
    </table> */
    
/*<div className="ag-theme-alpine" style={{ height: '400px' }}>
        <AgGridReact
          defaultColDef={{
            // set every column width
            width: 300,
            // make every column editable
            editable: true,
            // make every column use 'text' filter by default
            filter: 'agTextColumnFilter',
            resizable: true,
            sortable: true,
            // filter: true,
            headerComponentParams: {
                menuIcon: 'fa-bars'
            }
          }}
          rowSelection="multiple"
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}>
        </AgGridReact>
      </div>*/
  render() {
    const { content, recherche, resultSearch } = this.state
    const rechercheDOM = recherche && (
      <table className="table table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nom de la recherche</th>
          <th scope="col">Affichage</th>
          <th scope="col">Date de création</th>
        </tr>
      </thead>
      <tbody>
        { 
          recherche.map((item, index) => (
              <tr key={item.id_recherche} id={item.id_recherche} onClick={this.handeSearch}>
                <th scope="row">{index + 1 }</th>
                <td>{item.nom_recherche}</td>
                <td>{item.type_recherche}</td>
                <td>{item.date_recherche}</td>
              </tr>
            )
          )
        }
      </tbody>
    </table>
      
    )
    const resultSearchDOM = resultSearch && (
      <> 
        <br />
        <hr />
        <h1>Résultat de la recherche : </h1>
        <br />
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Etat</th>
              <th scope="col">Nom</th>
              <th scope="col">Description</th>
              <th scope="col">Emetteur</th>
              <th scope="col">Nature</th>
              <th scope="col">Indice</th>
              <th scope="col">Fichier</th>
            </tr>
          </thead>
          <tbody>
            {
              resultSearch.map((item, index) => (
                <tr key={item.id_document} id={item.id_document}>
                  <th scope="row"> {index + 1} </th>
                  <td> {item.etat_document} </td>
                  <td> {item.nom_document} </td>
                  <td> {item.description_document} </td>
                  <td> - </td>
                  <td> - </td>
                  <td> {item.indice_document} </td>
                  <td> - </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </>
    ) 
    return (
      <div className="container">
        <header className="jumbotron">
          {/* <h3><b>{content.nom + ' | '}</b><small>{content.description}</small></h3> */}
        </header>
        { rechercheDOM }
        { resultSearchDOM }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    operations : state.operations
  }
}
export default connect(mapStateToProps)(operation)