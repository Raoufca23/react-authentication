import React, { Component } from 'react'
import AuthService from "../services/auth.service";
import UserService from '../services/user.service'
import { connect } from 'react-redux'

class profile extends Component {

  constructor(props) {
    super(props)

    // const { cookies } = props;

    this.state = {
      // name: cookies.getAll(),
      currentUser: AuthService.getCurrentUser(),
      // operation : this.props.location.state.operations,
      currentOperation : undefined
    };
  }

  async componentDidMount() {
    console.log("cdm profile")
    // let currentUser = AuthService.getCurrentUser()
    let currentUser = this.state.currentUser
    await UserService.getAllOperations(currentUser.id_personne).then(result =>{

      const action = { type : "LIST_OPERATIONS", value: result.data }
      this.props.dispatch(action)

     /*  this.setState({
        currentUser,
        // operation : result.data
      }) */
    }).catch(e => console.log(e))
    /* this.setState({
      currentUser,
    }) */
    // console.log(this.state)
  }
  
  handleOperation = (e) => {
    // console.log(e.target.parentNode.id)
    // let  { currentOperation } = this.state
    let id_operation = parseInt(e.target.parentNode.id)
    let id_personne = this.state.currentUser.id_personne
    // let currentOperation = this.state.operation.find(item => item.id_operation === id_operation)
    let currentOperation = this.props.operations.find(item => item.id_operation === id_operation)

    const action = { type: "CURRENT_OPERATION", value : currentOperation }
    
    UserService.logPersonneToOperation({id_personne, id_operation}).then(res => {
      console.log(res)
      if(res.data.message === 'success') {

        this.props.dispatch(action)

        this.props.history.push({
          pathname: '/operation/' + id_operation,
        })

        /* this.setState({
          currentOperation
        },() => {
          console.log(this.state)
          this.props.history.push({
            pathname: '/operation/' + id_operation,
            state: { currentOperation: this.state.currentOperation }
          })
        }) */
      }
    })
  }
  
  render() {
    // console.log(this.props.operations)
    const { currentUser } = this.state;
    const operation = this.props.operations
    console.log(operation)
    const operationDOM = operation && (
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nom operation</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
          { 
            operation.map((item, index) => (
                <tr key={item.id_operation} id={item.id_operation} onClick={this.handleOperation}>
                  <th scope="row">{index + 1 }</th>
                  <td>{item.nom}</td>
                  <td colSpan="2">{item.description}</td>
                </tr>
              )
            )
          }
        </tbody>
      </table>
      )
    return (
      <div className="container">
        <header className="jumbotron">
          <h3>Hello {currentUser && currentUser.nom_personne + currentUser.prenom_personne}</h3>
        </header>
        {operationDOM}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    operations : state.operations
  }
}
export default connect(mapStateToProps)(profile)