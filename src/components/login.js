import React, { Component } from 'react'
import AuthService from '../services/auth.service'
import MD5 from 'md5'
import { connect } from 'react-redux'

class login extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      // email : '',
      login : '',
      password : '',
      loading: false,
      message: '',
      operations : ''
    }
  }
  
  onChangeEmail = (e) => {
    this.setState({
      login: e.target.value
    });
  }

  onChangePassword = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  submitForm = (e) => {
    e.preventDefault()
    var data = {
      login: this.state.login,
      password: MD5(this.state.password)
    };

    AuthService.connect(data)
      .then(response => {
        console.log(response)
        if(response.data.message === "success") {
          localStorage.setItem("user", JSON.stringify(response.data))
        }

        this.setState({
          operations : response.data.allOperations
        },() => {
          this.props.history.push({
            pathname: '/profile',
            state: { operations: this.state.operations }
          })
          // window.location.reload();
        })
      })
      .catch(error => {
        // console.error(error.response.data.message)
        console.log(error)
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          // email : '',
          login : '',
          password : '',
          loading: false,
          message: resMessage
        });
      })

  }

  render() {
    return (
      <div className="col-md-12">
        <div className="card card-container">
          <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />
          <form onSubmit={this.submitForm}>
            <div className="form-group">
              <label>Email :</label>
              <input className="form-control" type="text" placeholder="Enter your email" onChange={this.onChangeEmail} value={this.state.login} />
            </div>
            <div className="form-group">
              <label>Password :</label>
              <input className="form-control" type="password" placeholder="Enter your password" onChange={this.onChangePassword} value={this.state.password}/>
            </div>
            <div className="form-group">
              <button
                className="btn btn-primary btn-block"
                disabled={this.state.loading}
              >
                {this.state.loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Connect</span>
              </button>
            </div>
          </form>
          {
            this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    operations : state.operations
  }
}

export default connect(mapStateToProps)(login)