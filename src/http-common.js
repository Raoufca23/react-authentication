import axios from "axios";

export default axios.create({
  baseURL: "http://192.168.0.22:3001/api",
  // baseURL: "https://192.168.0.22:3001/api",
  headers: {
    "Content-type": "application/json"
  },
  withCredentials : true
});